import {removeUndefined} from './remove-undefined.helper';

describe('Remove undefined helper', () => {
    it('Should return a object with removed undefined keys', () =>{
        const dog = {name:'KIKA', color: 'BLACK', age:undefined }
        const dogNoAge = removeUndefined(dog);
        expect(dogNoAge).toEqual({name:'KIKA', color: 'BLACK'});
    })
});