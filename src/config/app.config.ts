export default () => ({
    globalPrefix: 'api',
    port: parseInt(process.env.APP_PORT,10) | 7000,
    jwtSecret: process.env.JWT_SECRET_KEY,
    jwtExpires: parseInt(process.env.JWT_EXPIRES, 10 ) | 3600
})