import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Transform } from 'class-transformer';

@Entity()
export class Tag {
  @PrimaryGeneratedColumn()
  id: number;

  @Transform((value:string) => value.toUpperCase)
  @Column({ name: 'name', length: 25, nullable: false, unique: true })
  name: string;

  constructor(partial: Partial<Tag>) {
    Object.assign(this, partial);
  }
}
