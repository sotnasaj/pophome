import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { UserStatus, UserRoles } from './enums/';
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Column({ name: 'first_name', nullable: false })
  firstName: string;

  @Column({ name: 'last_name', nullable: false })
  lastName: string;

  @Column({ name: 'email', nullable: false, unique: true })
  email: string;

  @Column({ name: 'birth_day', type: Date, nullable: false })
  birthday: Date;

  @Column({ name: 'is_active', default: UserStatus.ACTIVE, enum: UserStatus })
  status: string;

  @Column({ name: 'role', default: UserRoles.CLIENT, enum: UserRoles })
  role: string;

  @Exclude()
  @Column({ nullable: false })
  password: string;

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
