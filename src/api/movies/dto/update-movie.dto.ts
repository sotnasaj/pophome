import { MaxLength, IsOptional, IsUrl, IsArray } from 'class-validator';
import { Tag } from 'src/api/tags/tag.entity';

export class UpdateMovieDto {
  @IsOptional()
  @MaxLength(250)
  tittle: string;

  @IsOptional()
  @MaxLength(500)
  description: string;

  @IsOptional()
  @MaxLength(1500)
  synopsis: string;

  @IsOptional()
  @IsUrl()
  poster: string;

  @IsOptional()
  stock: number;

  @IsOptional()
  @IsUrl()
  trailer: string;

  @IsOptional()
  salePrice: number;

  @IsOptional()
  rentPrice: number;

  @IsOptional()
  releaseDate: Date;

  @IsOptional()
  @IsArray()
  tags: Tag[];
}
