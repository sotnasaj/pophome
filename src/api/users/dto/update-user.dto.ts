import { IsNotEmpty, IsEmail, IsOptional } from 'class-validator';

export class UpdateUserDto {

    @IsOptional()
    @IsNotEmpty()
    firstName: string;

    @IsOptional()
    @IsNotEmpty()
    lastName: string;
    
    @IsEmail()
    email: string;
    
}