import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { MoviesModule } from './movies/movies.module';
import { TagsModule } from './tags/tags.module';
import { LikesModule } from './likes/likes.module';

@Module({
  imports: [UsersModule, MoviesModule, TagsModule, LikesModule],
})
export class ApiModule {}
