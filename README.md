<p align="center">
 <a href="https://gitlab.com/sotnasaj/ja3pi/" target="blank"><img src="https://i.pinimg.com/originals/60/46/90/604690618500e27af1318eb9756be021.png" width="175" alt="PopHome logo" /></a>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="420" alt="PopHome Logo" /></a>
</p>

## Made with [...]

## Description

Small movies rental API.

## Running the app

```bash
# development
$ npm start

# watch mode
$ npm run start:dev

```
## Usage ( <3 SWAGGER!!!! )

Locally visit:

http://localhost:7000/api/pophome/docs/

Create user [Post] does not require any authentication, so you can create your own user ! 
For example purposes users email and password are related (at least 4,5,6)

username4@email.com - password4
username5@email.com - password5
username6@email.com - password6

and an Admin: admin@email.com - adminpass

## Running test

### Unit test --> out dir ./coverage (on root)
npm test
### Unit test --> out dir ./coverage-e2e (on root)
 npm run test:e2e 




but you can do whatever you want.