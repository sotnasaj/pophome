import { Controller,Get,Post,Body,UsePipes,ValidationPipe,UseGuards } from '@nestjs/common';
import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { AdminGuard } from '../../auth/guards/admin.guard';

@ApiBearerAuth()
@UseGuards(AdminGuard)
@ApiTags('Tags')
@Controller('tags')
export class TagsController {
  constructor(private readonly _tagsService: TagsService) {}

  @Get()
  findAll() {
    return this._tagsService.findAll();
  }

  @UsePipes(new ValidationPipe())
  @Post()
  addOne(@Body() tag: CreateTagDto) {
    return this._tagsService.insertOne(tag);
  }
}
