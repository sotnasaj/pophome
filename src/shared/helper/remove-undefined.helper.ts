export function removeUndefined(source: any) {
  const newObject = Object.keys(source).reduce((acc, key) => {
    if (source[key] !== undefined) acc[key] = source[key];
    return acc;
  }, {});
  return newObject;
}