import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { fakeUser } from '../../../test/helpers/fake.util';

describe('UsersService', () => {
  let service: UsersService;
  let repo: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();
    service = module.get<UsersService>(UsersService);
    repo = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe(`Get /users/`, () => {
    it(`should return an array of user[] if any`, () => {
      const mockUserArray = [
        fakeUser({ id: 25, firstName: 'Jane', lastName: 'Doe' }),
        fakeUser({ id: 26, firstName: 'John', lastName: 'Doe' }),
      ];
      jest.spyOn(repo, 'find').mockResolvedValueOnce(mockUserArray);
      expect(service.findAll()).resolves.toEqual(mockUserArray);
    });
    it(`it should return an empty array if not any`, () => {
      jest.spyOn(repo, 'find').mockResolvedValueOnce([]);
      expect(service.findAll()).resolves.toEqual([]);
    });
  });

  describe(`Get /users/:id)`, () => {
    it(`should return an User if exist`, () => {
      const mockUser = fakeUser({ id: 25, firstName: 'Jane', lastName: 'Doe' });
      const spyRepo = jest
        .spyOn(repo, 'findOne')
        .mockResolvedValueOnce(mockUser);
      expect(service.findById(25)).resolves.toEqual(mockUser);
      expect(spyRepo).toHaveBeenCalledWith({ id: 25 });
    });
    it(`should return undefined if user does not exist`, () => {
      const spyRepo = jest
        .spyOn(repo, 'findOne')
        .mockResolvedValueOnce(undefined);
      expect(service.findById(4444)).resolves.toBeUndefined();
      expect(spyRepo).toHaveBeenCalledWith({ id: 4444 });
    });
  });

  describe(`Post /users`, () => {
    const password = 'lopez';
    const user = fakeUser({ id: 1, firstName: 'Javier', password });
    it(`should post and return an user`, () => {
      jest.spyOn(repo, 'create').mockReturnValueOnce(user);
      jest.spyOn(repo, 'save').mockImplementationOnce(async (result: User) => {
        return result;
      });
      expect(
        service.insertOne({
          birthday: user.birthday,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          password: user.password,
          role: user.role,
        }),
      ).resolves.toEqual(user);
    });
  });

  describe(`Delete /users/:id`, () => {
    it(`should remove and return {deleted:true}`, () => {
      jest.spyOn(repo,'delete').mockResolvedValue({affected:1,raw:'ok'});
      expect(service.deleteById(1)).resolves.toEqual({ deleted: true });
    });
    it(`should fail and return {deleted:false}`, () => {
      jest.spyOn(repo,'delete').mockResolvedValue({affected:0,raw:'fail'});
      expect(service.deleteById(1)).resolves.toEqual({ deleted: false });
    });
  });
});
