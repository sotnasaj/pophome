import { IsNotEmpty, IsEmail, IsOptional, IsIn, MinLength, MaxLength} from 'class-validator';

export class CreateUserDto {

    @IsNotEmpty()
    firstName: string;

    @IsNotEmpty()
    lastName: string;

    @IsEmail()
    email: string;

    birthday: Date;

    @IsOptional()
    @IsIn(['client','admin'])
    role: string;

    @IsNotEmpty()
    @MinLength(8)
    @MaxLength(25)
    password: string;

}