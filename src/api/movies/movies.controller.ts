import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  NotFoundException,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Patch,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { MoviesService } from './movies.service';
import { from, Observable } from 'rxjs';
import { Movie } from './movie.entity';
import { map } from 'rxjs/operators';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { AdminGuard } from '../../auth/guards/admin.guard';
import { ClientGuard } from 'src/auth/guards/client.guard';
import { IJwtPayload } from 'src/auth/interfaces/jwt-payload.interface';

@ApiTags('Movies')
@Controller('movies')
export class MoviesController {
  constructor(private readonly _moviesService: MoviesService) {}

  @Get()
  getMovies(): Observable<Movie[]> {
    return from(this._moviesService.findAll());
  }

  @Get(':id')
  getMovieById(@Param('id', ParseIntPipe) id: number) {
    return from(this._moviesService.findById(id)).pipe(
      map(response => {
        if (!response) throw new NotFoundException('Movie not found');
        return response;
      }),
    );
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  @Post()
  addMovie(@Body() movie: CreateMovieDto) {
    return from(this._moviesService.insertOne(movie));
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  @Patch(':id')
  updateById(
    @Param('id', ParseIntPipe) id: number,
    @Body() partialMovie: UpdateMovieDto,
  ): Observable<Movie> {
    return from(this._moviesService.updateById(id, partialMovie)).pipe(
      map(result => {
        if (!result) throw new NotFoundException();
        return result;
      }),
    );
  }

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Delete(':id')
  deleteById(
    @Param('id', ParseIntPipe) id: number,
  ): Observable<{ deleted: boolean }> {
    return from(this._moviesService.deleteById(id));
  }

  @ApiBearerAuth()
  @UseGuards(ClientGuard)
  @Post('/:id/likes/add-like')
  addLike(@Param('id', ParseIntPipe) id: number, @Req() request: Request) {
    const user = request.user as IJwtPayload;
    return from(this._moviesService.likeMovieById(id, user.userId)).pipe(
      map(result => {
        if(!result) throw new NotFoundException('User o movie does not exist');
        return result;
      })
    );
  }
}
