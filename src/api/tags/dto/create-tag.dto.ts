import { Length } from 'class-validator';
export class CreateTagDto {
  @Length(1, 25)
  name: string;
}