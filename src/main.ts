import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule } from '@nestjs/swagger';
import { swaggerDocument } from './config/swagger-document';
import { QueryFailExceptionFilter } from './shared/filters/query-fail-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  app.setGlobalPrefix(config.get<string>('app.globalPrefix'));
  app.useGlobalFilters(new QueryFailExceptionFilter());
  app.enableCors();
  app.use(helmet());
  const document = SwaggerModule.createDocument(app, swaggerDocument);
  SwaggerModule.setup('api/pophome/docs', app, document);
  await app.listen(config.get<number>('app.port'));
}
bootstrap();
