import {
  Controller,
  ValidationPipe,
  UsePipes,
  Post,
  Body,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import LoginUserDto from '../api/users/dto/login-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly _authService: AuthService) {}

  @Post('login')
  @UsePipes(new ValidationPipe())
  async login(@Body() user: LoginUserDto) {
    const token = await this._authService.validate(user);
    return token;
  }
}
