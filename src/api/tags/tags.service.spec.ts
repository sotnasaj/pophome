import { Test, TestingModule } from '@nestjs/testing';
import { TagsService } from './tags.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Tag } from './tag.entity';
import { Repository } from 'typeorm';

describe('TagsService', () => {
  let service: TagsService;
  let repo: Repository<Tag>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TagsService,
        {
          provide: getRepositoryToken(Tag),
          useClass: Repository,
        },
      ],
    }).compile();
    service = module.get<TagsService>(TagsService);
    repo = module.get<Repository<Tag>>(getRepositoryToken(Tag));
  });

  it('should be defined (compiled)', () => {
    expect(service).toBeDefined();
  });

  describe(`Get /tags`, () => {
    it(`should return empty array if not any`, () => {
      jest.spyOn(repo, 'find').mockResolvedValueOnce([]);
      expect(service.findAll()).resolves.toEqual([]);
    });
    it(`should return an array of tags if any`, () => {
      const mockTag = new Tag({ id: 1, name: 'RANDOM' });
      jest.spyOn(repo, 'find').mockResolvedValueOnce([mockTag]);
      expect(service.findAll()).resolves.toEqual([mockTag]);
    });
  });
  describe(`Post /tags`, () => {
    const mockTag = new Tag({ id: 1, name: 'RANDOM' });
    it(`should post and return a tag`, () => {
      jest.spyOn(repo, 'create').mockReturnValueOnce(mockTag);
      jest.spyOn(repo, 'save').mockImplementationOnce(async (tag: Tag) => {
        return tag;
      });
      expect(service.insertOne({ name: 'RANDOM' })).resolves.toEqual(mockTag);
    });
  });
});
