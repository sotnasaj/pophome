import { join } from 'path';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default () =>
  ({
    type: process.env.DB_TYPE || ('postgres' as any),
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_DATABASE || 'local_db',
    entities: [join(__dirname, '../', 'api/**/*.entity{.ts,.js}')],
    dropSchema: process.env.DB_DROP_SCHEMA === 'true',
    synchronize: true,
  } as TypeOrmModuleOptions);
