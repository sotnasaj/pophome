import { QueryFailedError } from 'typeorm';
import { ArgumentsHost, ExceptionFilter, HttpStatus, Catch } from '@nestjs/common';
import { Response } from 'express'

@Catch(QueryFailedError)
export class QueryFailExceptionFilter implements ExceptionFilter {
    catch(exception: QueryFailedError, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse<Response>();
        const request = context.getRequest<Request>();
        const { url } = request;
        const errorResponse = {
            path: url,
            timestamp: new Date().toISOString(),
            message: 'Seems to be some data conflicts'
        };
        response.status(HttpStatus.CONFLICT).json(errorResponse);
    }
}
