import { DocumentBuilder } from "@nestjs/swagger";

export const swaggerDocument = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('PopHome API')
    .setDescription('Movies and Popcorn (Emotions included) ')
    .setVersion('0.1')
    .addTag('pophome')
    .build();