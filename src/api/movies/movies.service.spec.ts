import { Test, TestingModule } from '@nestjs/testing';
import { MoviesService } from './movies.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Movie } from './movie.entity';
import { Repository } from 'typeorm';
import { fakeMovie } from '../../../test/helpers/fake.util';
import { User } from '../users/user.entity';
import  {Like} from './../likes/like.entity';

describe('MoviesService', () => {
  let service: MoviesService;
  let repo: Repository<Movie>;
  let usersRepo: Repository<User>;
  let likesRepo: Repository<Like>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        {
          provide: getRepositoryToken(Movie),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Like),
          useClass: Repository,
        },
      ],
    }).compile();
    service = module.get<MoviesService>(MoviesService);
    repo = module.get<Repository<Movie>>(getRepositoryToken(Movie));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe(`Get /movies/`, () => {
    it(`should return an array of movies if any`, () => {
      const movies = [
        fakeMovie({ id: 25, tittle: '101 Dalmatas' }),
        fakeMovie({ id: 26, tittle: 'Noche de desvelo' }),
      ];
      jest.spyOn(repo, 'find').mockResolvedValueOnce(movies);
      expect(service.findAll()).resolves.toEqual(movies);
    });
    it(`it should return an empty array if not any`, () => {
      jest.spyOn(repo, 'find').mockResolvedValueOnce([]);
      expect(service.findAll()).resolves.toEqual([]);
    });
  });
  describe(`Get /movies/:id`, () => {
    it(`should return an movie if exist`, () => {
      const movie = fakeMovie({ id: 25, tittle: 'Faker Backdoor' });
      jest.spyOn(repo, 'findOne').mockResolvedValueOnce(movie);
      expect(service.findById(25)).resolves.toEqual(movie);
    });
    it(`it should return undefined if not exist`, () => {
      jest.spyOn(repo, 'findOne').mockResolvedValueOnce(undefined);
      expect(service.findById(25)).resolves.toEqual(undefined);
    });
  });
  describe(`Post /movies`, () => {
    const movie = fakeMovie({ id: 25, tittle: 'She...' });
    it(`should post and return an user`, () => {
      jest.spyOn(repo, 'create').mockReturnValueOnce(movie);
      jest.spyOn(repo, 'save').mockImplementationOnce(async (result: Movie) => {
        return result;
      });
      expect(
        service.insertOne(movie),
      ).resolves.toEqual(movie);
    });
  });

  describe(`Delete /movies/:id`, () => {
    it(`should remove and return {deleted:true}`, () => {
      jest.spyOn(repo, 'delete').mockResolvedValue({ affected: 1, raw: 'ok' });
      expect(service.deleteById(25)).resolves.toEqual({ deleted: true });
    });
    it(`should fail and return {deleted:false}`, () => {
      jest
        .spyOn(repo, 'delete')
        .mockResolvedValue({ affected: 0, raw: 'fail' });
      expect(service.deleteById(444)).resolves.toEqual({ deleted: false });
    });
  });
});
