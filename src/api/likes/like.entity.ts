import { Entity, ManyToOne } from 'typeorm';
import { User } from '../users/user.entity';
import { Movie } from '../movies/movie.entity';

@Entity()
export class Like {
  @ManyToOne(() => User, { primary: true })
  user: User;

  @ManyToOne(() => Movie, { primary: true })
  movie: Movie;

  constructor(partial: Partial<Like>) {
    Object.assign(this, partial);
  }
}
