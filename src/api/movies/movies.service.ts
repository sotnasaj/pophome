import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movie } from './movie.entity';
import { Repository, Connection } from 'typeorm';
import { CreateMovieDto, UpdateMovieDto } from './dto';
import { removeUndefined } from '../../shared/helper/remove-undefined.helper';
import { Like } from '../likes/like.entity';
import { User } from '../users/user.entity';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private readonly _moviesRepository: Repository<Movie>,
    @InjectRepository(Like)
    private readonly _likesRepository: Repository<Like>,
    @InjectRepository(User)
    private readonly _usersRepository: Repository<User>,
    private readonly connection: Connection,
  ) {}

  async findAll(): Promise<Movie[]> {
    return this._moviesRepository.find({ relations: ['tags'] });
  }

  async findById(id: number): Promise<Movie> {
    return this._moviesRepository.findOne({ id }, { relations: ['tags'] });
  }

  async insertOne(movie: CreateMovieDto): Promise<Movie> {
    const newMovie = this._moviesRepository.create(movie);
    return this._moviesRepository.save(newMovie);
  }

  async deleteById(id: number): Promise<{ deleted: boolean }> {
    const result = await this._moviesRepository.delete({ id });
    if (result.affected > 0) return { deleted: true };
    return { deleted: false };
  }

  async updateById(
    id: number,
    partialMovie: UpdateMovieDto,
  ): Promise<Movie> | undefined {
    const movieUpdates = removeUndefined(partialMovie);
    const movie = await this._moviesRepository.findOne({ id });
    if (!movie) return;
    const newMovie = Object.assign({}, movie, movieUpdates);
    return await this._moviesRepository.save(newMovie);
  }

  async likeMovieById(
    movieId: number,
    userId: number,
  ): Promise<{ added: boolean }> {
    const movie = await this._moviesRepository.findOne({ id: movieId });
    const user = await this._usersRepository.findOne({ id: userId });
    if(!movie || !user) return undefined;
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await queryRunner.manager.createQueryBuilder()
      .insert()
      .into(Like)
      .values({movie: movie, user: user})
      .execute();
      await queryRunner.manager.save(movie);
      await queryRunner.commitTransaction();
      return { added: true };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      return { added: false };
    } finally {
      await queryRunner.release();
    }
  }
}
