import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Tag } from '../tags/tag.entity';

@Entity()
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'tittle', length: 250 })
  tittle: string;

  @Column({ name: 'descriptions', length: 500, nullable: true })
  description: string;

  @Column({ name: 'synopsis', length: 1500, nullable: true })
  synopsis: string;

  @Column({ name: 'poster', nullable: true })
  poster: string;

  @Column({ name: 'stock', nullable: false, default: 0 })
  stock: number;

  @Column({ name: 'trailer', nullable: true })
  trailer: string;

  @Column({
    name: 'sale_price',
    type: 'numeric',
    nullable: true,
    precision: 5,
    scale: 2,
  })
  salePrice: number;

  @Column({
    name: 'rent_price',
    type: 'numeric',
    nullable: true,
    precision: 5,
    scale: 2,
  })
  rentPrice: number;

  @Column({ name: 'likes_count', type: 'numeric', default: 0 })
  likesCount: number;

  @Column({ name: 'release_date', type: Date })
  releaseDate: Date;

  @ManyToMany(() => Tag)
  @JoinTable()
  tags: Tag[];

  constructor(partial: Partial<Movie>) {
    Object.assign(this, partial);
  }
}
