import { User } from '../../src/api/users/user.entity';
import * as Faker from 'faker';
import { UserStatus, UserRoles } from '../../src/api/users/enums';
import { Movie } from '../../src/api/movies/movie.entity';

export function fakeUser(user: Partial<User>) {
  return new User({
    id: user.id ? user.id : undefined,
    firstName: user.firstName ? user.firstName : Faker.name.firstName(),
    lastName: user.lastName ? user.lastName : Faker.name.lastName(),
    birthday: user.birthday ? user.birthday : Faker.date.past(2012),
    email: user.email ? user.email : Faker.internet.email(),
    status: user.status ? user.status : UserStatus.ACTIVE,
    password: user.password ? user.password : Faker.internet.password(10),
    role: user.role ? user.role : UserRoles.CLIENT,
  });
}

export function fakeMovie(movie: Partial<Movie>): Movie {
  return new Movie({
    id: movie.id ? movie.id : undefined,
    description: movie.description ? movie.description : Faker.lorem.sentence(),
    likesCount: movie.likesCount ? movie.likesCount : 0,
    poster: movie.poster ? movie.poster : Faker.internet.url(),
    releaseDate: movie.releaseDate ? movie.releaseDate : Faker.date.past(2019),
    rentPrice: movie.rentPrice ? movie.rentPrice : Faker.random.number(70),
    salePrice: movie.salePrice ? movie.salePrice : Faker.random.number(150),
    stock: movie.stock ? movie.stock : 0,
    synopsis: movie.synopsis ? movie.synopsis : Faker.lorem.paragraph(5),
    tittle: movie.tittle ? movie.tittle : Faker.random.words(3),
    trailer: movie.trailer ? movie.trailer : Faker.internet.url(),
    tags: movie.tags ? movie.tags : [{ name: 'FAKE', id: 1 }],
  });
}
