import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class AdminGuard extends AuthGuard('jwt') {
  handleRequest(err, user) {

    const {role} = user;
    if (err || !user || role!=='admin') {
      throw new UnauthorizedException();
    }
    return user;
  }
}
