import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IJwtPayload } from '../interfaces/jwt-payload.interface';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../../api/users/users.service';
import { UserStatus } from '../../api/users/enums';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly _userService: UsersService,
    private readonly _configService: ConfigService,
  ) {
    super({
      // We are gonna use bearer auth header
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: _configService.get('app.jwtSecret'),
    });
  }

  async validate(payload: IJwtPayload) {
    const user = await this._userService.findById(payload.userId);
    if (!user || user.status == UserStatus.BANNED) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return payload;
  }
}
