import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Tag } from './tag.entity';
import { Repository } from 'typeorm';
import { CreateTagDto } from './dto/create-tag.dto';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private readonly _tagsRepository: Repository<Tag>,
  ) {}

  async findAll() {
    return this._tagsRepository.find();
  }

  async insertOne(tag: CreateTagDto) {
    const newTag = this._tagsRepository.create(tag);
    return this._tagsRepository.save(newTag);
  }
}