import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { fakeUser } from './helpers/fake.util';

describe('Auth (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Post auht/login', () => {
    beforeAll(async () => {
      const user = fakeUser({
        email: 'user@email.com',
        password: 'userPass',
      });
      await request(app.getHttpServer())
        .post('/users')
        .send(user);
    });

    it('should fail login 401', async () => {
      const loginResponse = await request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'bademail@email.com', password: 'badPass' })
        .expect(401);
      const { accessToken } = loginResponse.body;
      expect(accessToken).toBeUndefined();
    });

    it('should log and retrieve a token', async () => {
      const loginResponse = await request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'user@email.com', password: 'userPass' })
        .expect(201);
      const { accessToken } = loginResponse.body;
      expect(accessToken).toBeDefined();
    });
  });
});
