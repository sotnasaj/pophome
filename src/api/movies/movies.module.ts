import { Module } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { MoviesController } from './movies.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Movie } from './movie.entity';
import { LikesModule } from '../likes/likes.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([Movie]),LikesModule, UsersModule],
  providers: [MoviesService],
  controllers: [MoviesController],
})
export class MoviesModule {}
