import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  NotFoundException,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Patch,
  UseGuards,
  ClassSerializerInterceptor,
  UseInterceptors,
  Req,
  ForbiddenException,
  Delete,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { Request } from 'express';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './dto';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { AdminGuard } from '../../auth/guards/admin.guard';
import { ClientGuard } from '../../auth/guards/client.guard';
import { IJwtPayload } from 'src/auth/interfaces/jwt-payload.interface';

@ApiTags('Users')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('users')
export class UsersController {
  constructor(private readonly _userService: UsersService) {}

  @ApiBearerAuth()
  @UseGuards(AdminGuard)
  @Get()
  getUsers(): Observable<User[]> {
    return from(this._userService.findAll());
  }

  @ApiBearerAuth()
  @UseGuards(ClientGuard)
  @Get(':id')
  getUserById(
    @Param('id', ParseIntPipe) id: number,
    @Req() request: Request,
  ): Observable<User> {
    const user = request.user as IJwtPayload;
    if (id !== user.userId) throw new ForbiddenException('Permission denied');
    return from(this._userService.findById(id)).pipe(
      map(user => {
        if (!user) throw new NotFoundException('User not found');
        return user;
      }),
    );
  }

  @UsePipes(new ValidationPipe({ transform: true }))
  @Post()
  addUser(@Body() user: CreateUserDto) {
    return from(this._userService.insertOne(user));
  }

  @ApiBearerAuth()
  @UseGuards(ClientGuard)
  @Patch(':id')
  updateUserById(
    @Param('id', ParseIntPipe) id: number,
    @Body() partialUser: UpdateUserDto,
    @Req() request: Request,
  ) {
    const user = request.user as IJwtPayload;
    if (id !== user.userId) throw new ForbiddenException('Permission denied');
    return from(this._userService.updateById(id, partialUser)).pipe(
      map(user => {
        if (!user) throw new NotFoundException('User not found');
        return user;
      }),
    );
  }

  @ApiBearerAuth()
  @UseGuards(ClientGuard)
  @Delete(':id')
  deleteById(
    @Param('id', ParseIntPipe) id: number,
    @Req() request: Request,
  ): Observable<{ deleted: boolean }> {
    const user = request.user as IJwtPayload;
    if (id !== user.userId) throw new ForbiddenException('Permission denied');
    return from(this._userService.deleteById(id));
  }
}