import { Injectable, UnauthorizedException, ConflictException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserStatus, UserRoles } from './enums';
import { CreateUserDto, UpdateUserDto } from './dto';
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import LoginUserDto from './dto/login-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly _usersRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this._usersRepository.find();
  }

  async findById(id: number): Promise<User> {
    return this._usersRepository.findOne({ id });
  }

  async findBy(query: {}): Promise<User> {
    return this._usersRepository.findOne(query);
  }

  async insertOne(user: CreateUserDto): Promise<User> {
    const userMail = await this._usersRepository.findOne({ email: user.email });
    if (userMail) throw new ConflictException('Email already in use');;
    const newUser = this._usersRepository.create(user);
    newUser.password = hashSync(newUser.password, genSaltSync());
    return this._usersRepository.save(newUser);
  }

  async updateById(id: number, user: UpdateUserDto) {
    await this._usersRepository.update({ id }, user);
    return this.findById(id);
  }

  async deleteById(id: number): Promise<{ deleted: boolean }> {
    const result = await this._usersRepository.delete({ id });
    if (result.affected > 0) return { deleted: true };
    return { deleted: false };
  }

  async updateUserStatus(id: number, status: UserStatus): Promise<User> {
    await this._usersRepository
      .createQueryBuilder()
      .update()
      .set({ status: status })
      .where({ id })
      .execute();
    return this.findById(id);
  }

  async updateUserRole(id: number, role: UserRoles): Promise<User> {
    await this._usersRepository
      .createQueryBuilder()
      .update()
      .set({ role: role })
      .where({ id })
      .execute();
    return this.findById(id);
  }

  async login(userCredentials: LoginUserDto): Promise<User> {
    const user = await this.findBy({
      email: userCredentials.email,
    });
    if (!user) throw new UnauthorizedException('Invalid username/password');
    const verifiedPassword = compareSync(
      userCredentials.password,
      user.password,
    );
    if (!verifiedPassword)
      throw new UnauthorizedException('Invalid username/password');

    return user;
  }
}
