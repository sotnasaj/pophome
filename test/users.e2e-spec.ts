import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { fakeUser } from './helpers/fake.util';

describe('User controller (e2e)', () => {
  let app: INestApplication;
  const sampleUser = fakeUser({
    id: 1,
    firstName: 'Javier',
    password: 'password',
  });

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Any guest', () => {
    it('should post a User 201', async () => {
      const postResponse: any = await request(app.getHttpServer())
        .post('/users')
        .send(sampleUser)
        .expect(201);
      expect(postResponse.body.id).toEqual(expect.any(Number));
      expect(postResponse.body.password).toBeUndefined();
    });
  });

  describe('As admin user', () => {
    let adminToken: string;

    beforeAll(async () => {
      const adminUser = fakeUser({
        id: 2,
        email: 'admin@email.com',
        password: 'adminPass',
        role: 'admin',
      });
      await request(app.getHttpServer())
        .post('/users')
        .send(adminUser);
      const loginResponse = await request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'admin@email.com', password: 'adminPass' });
      adminToken = loginResponse.body.accessToken;
    });

    it('should be allowed to get /users', () => {
      return request(app.getHttpServer())
        .get('/users')
        .set('Authorization', 'Bearer ' + adminToken)
        .expect(200);
    });

    it('should be allowed to get /user/:id', async () => {
      const getResponse = await request(app.getHttpServer())
        .get('/users/1')
        .set('Authorization', 'Bearer ' + adminToken)
        .expect(200);
      expect(getResponse.body.id).toEqual(expect.any(Number));
      expect(getResponse.body.password).toBeUndefined();
    });

    it('should be allowed to delete /users/:id', () => {
      return request(app.getHttpServer())
        .get('/users/1')
        .set('Authorization', 'Bearer ' + adminToken)
        .expect(200);
    });
  });
});