import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { fakeUser, fakeMovie } from './helpers/fake.util';
import { Tag } from '../src/api/tags/tag.entity';

describe('Movies controller (e2e)', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });
  describe('As an admin user', () => {
    let adminToken: string;
    let sampleTag: Tag;

    beforeAll(async () => {
      const adminUser = fakeUser({
        id: 1,
        email: 'admin@email.com',
        password: 'adminPass',
        role: 'admin',
      });
      await request(app.getHttpServer())
        .post('/users')
        .send(adminUser);
      const loginResponse = await request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'admin@email.com', password: 'adminPass' });
      adminToken = loginResponse.body.accessToken;
      sampleTag = new Tag({ id: 1, name: 'TAG1' });
      await request(app.getHttpServer())
        .post('/tags')
        .set('Authorization', 'Bearer ' + adminToken)
        .send(sampleTag);
    });

    it('should be allowed to post /movies', () => {
      const sampleMovie = fakeMovie({
        id: 1,
        tittle: 'The clock is ticking',
        tags: [sampleTag],
      });
      return request(app.getHttpServer())
        .post('/movies')
        .set('Authorization', 'Bearer ' + adminToken)
        .send(sampleMovie)
        .expect(201);
    });

    it('should be allowed to delete /movies/:id', () => {
      return request(app.getHttpServer())
        .delete('/movies/1')
        .set('Authorization', 'Bearer ' + adminToken)
        .expect(200)
        .expect({ deleted: true });
    });
  });

  describe('As an client user', () => {
    let clientToken: string;

    beforeAll(async () => {
      const clientUser = fakeUser({
        id: 1,
        email: 'client@email.com',
        password: 'clientPass',
        role: 'client',
      });
      await request(app.getHttpServer())
        .post('/users')
        .send(clientUser);
      const loginResponse = await request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'client@email.com', password: 'clientPass' });
      clientToken = loginResponse.body.accessToken;
    });

    it('should not be allowed to post /movies', () => {
      const sampleMovie = fakeMovie({
        tittle: 'The clock is ticking',
        description: 'Time in a bottle',
      });
      return request(app.getHttpServer())
        .post('/movies')
        .set('Authorization', 'Bearer ' + clientToken)
        .send(sampleMovie)
        .expect(401);
    });

    it('should not be allowed to delete /movies/:id', () => {
      return request(app.getHttpServer())
        .delete('/movies/1')
        .set('Authorization', 'Bearer ' + clientToken)
        .expect(401);
    });
  });

  describe('Any guest', () => {
    it('should be allowed to get /movies ', async () => {
      await request(app.getHttpServer())
        .get('/movies')
        .expect(200);
    });
    it('should receive a 404 if no /movies/:id', async () => {
      await request(app.getHttpServer())
        .get('/movies/1')
        .expect(404);
    });
  });
});
