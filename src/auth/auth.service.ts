import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../../src/api/users/users.service';
import { IJwtPayload } from './interfaces/jwt-payload.interface';
import LoginUserDto from '../api/users/dto/login-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly _jwtService: JwtService,
    private readonly _userService: UsersService,
  ) {}

  async validate(
    userCredentials: LoginUserDto,
  ): Promise<{ accessToken: string }> {
    const user = await this._userService.login(userCredentials);
    const payload: IJwtPayload = {
      userId: user.id,
      role: user.role,
    };
    return { accessToken: this._jwtService.sign(payload) };
  }
}
